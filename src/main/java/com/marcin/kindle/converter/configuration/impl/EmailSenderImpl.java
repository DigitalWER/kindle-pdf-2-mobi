package com.marcin.kindle.converter.configuration.impl;

import com.google.api.services.gmail.Gmail;
import com.google.api.services.gmail.model.Message;
import com.marcin.kindle.converter.configuration.EmailSender;
import jakarta.activation.DataHandler;
import jakarta.activation.DataSource;
import jakarta.activation.FileDataSource;
import jakarta.mail.BodyPart;
import jakarta.mail.MessagingException;
import jakarta.mail.Multipart;
import jakarta.mail.Session;
import jakarta.mail.internet.InternetAddress;
import jakarta.mail.internet.MimeBodyPart;
import jakarta.mail.internet.MimeMessage;
import jakarta.mail.internet.MimeMultipart;
import lombok.extern.slf4j.Slf4j;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Base64;
import java.util.Properties;

import static com.marcin.kindle.converter.context.EmailSenderContext.kindleDeviceEmail;
import static com.marcin.kindle.converter.context.FileContext.outputFile;


@Slf4j
public class EmailSenderImpl implements EmailSender {
    public Message createEmail() {
        log.info("Creating an email.");
        MimeMessage emailContent = new MimeMessage(Session.getDefaultInstance(new Properties(), null));
        try {
            InternetAddress senderAddress = new InternetAddress("me");
            InternetAddress receiverAddress = new InternetAddress(kindleDeviceEmail);
            log.info("Sender: {}, Receiver: {}", senderAddress, receiverAddress);

            emailContent.setFrom(senderAddress);
            emailContent.setRecipient(jakarta.mail.Message.RecipientType.TO, receiverAddress);
            emailContent.setSubject("test");

            Multipart multipart = new MimeMultipart();
            BodyPart bodyPart = new MimeBodyPart();
            bodyPart.setContent("test", "text/plain");
            multipart.addBodyPart(bodyPart);

            BodyPart attachmentBodyPart = new MimeBodyPart();
            DataSource source = new FileDataSource(outputFile);
            attachmentBodyPart.setDataHandler(new DataHandler(source));
            attachmentBodyPart.setFileName(new File(outputFile).getName());
            multipart.addBodyPart(attachmentBodyPart);

            emailContent.setContent(multipart);
        } catch (MessagingException e) {
            e.printStackTrace();
        }

        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        try {
            emailContent.writeTo(buffer);
        } catch (IOException | MessagingException e) {
            e.printStackTrace();
        }

        byte[] bytes = buffer.toByteArray();
        var encodedEmail = Base64.getUrlEncoder().encodeToString(bytes);

        return new Message().setRaw(encodedEmail);
    }

    public void sendEmail(Gmail service) throws IOException {
        var message = createEmail();
        service.users().messages().send("me", message).execute();
        log.info("Email sent!");
    }
}
