package com.marcin.kindle.converter.configuration;

import com.google.api.services.gmail.Gmail;
import com.google.api.services.gmail.model.Message;

import java.io.IOException;

public interface EmailSender {
    Message createEmail();

    void sendEmail(Gmail service) throws IOException;
}
