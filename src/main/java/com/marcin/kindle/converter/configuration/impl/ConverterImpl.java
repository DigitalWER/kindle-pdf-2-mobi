package com.marcin.kindle.converter.configuration.impl;

import com.marcin.kindle.converter.configuration.Converter;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import static com.marcin.kindle.converter.context.FileContext.getFileName;
import static com.marcin.kindle.converter.context.FileContext.getFullFile;
import static com.marcin.kindle.converter.context.FileContext.inputFileDirectory;
import static com.marcin.kindle.converter.context.FileContext.outputDirectory;
import static com.marcin.kindle.converter.context.FileContext.outputFile;

@Slf4j
@NoArgsConstructor
public class ConverterImpl implements Converter {
    public void pdfToMobi() {
        log.info("Performing conversion from .pdf to .mobi file");
        try {
            File inputFile = new File(inputFileDirectory);
            var pdfFile = PDDocument.load(inputFile);
            PDFTextStripper pdfStripper = new PDFTextStripper();
            var text = pdfStripper.getText(pdfFile);
            outputFile = outputDirectory + "\\" + getFileName(getFullFile(inputFile)) + ".mobi";
            FileOutputStream mobiFile = new FileOutputStream(outputFile);
            mobiFile.write(text.getBytes());
            mobiFile.close();
            pdfFile.close();
            log.info("Conversion completed!");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
