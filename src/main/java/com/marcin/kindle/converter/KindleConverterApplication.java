package com.marcin.kindle.converter;

import com.marcin.kindle.converter.windows.MainWindow;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KindleConverterApplication extends MainWindow {

	public static void main(String[] args) {
		SpringApplication.run(KindleConverterApplication.class, args);
	}
}
