package com.marcin.kindle.converter.windows;

import com.marcin.kindle.converter.configuration.impl.EmailSenderImpl;
import com.marcin.kindle.converter.configuration.oauth.GmailOAuth2;
import lombok.Getter;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.WindowConstants;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static com.marcin.kindle.converter.context.EmailSenderContext.kindleDeviceEmail;

public class EmailWindow extends JFrame implements ActionListener {

    private final JTextField recipientField;
    @Getter
    private boolean emailDataEntered;

    public EmailWindow() {
        setTitle("Email Information");
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setLayout(new GridBagLayout());
        setPreferredSize(new Dimension(400, 200));
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.insets = new Insets(5, 5, 5, 5);

        JLabel recipientLabel = new JLabel("Recipient:");
        constraints.gridx = 0;
        constraints.gridy = 0;
        add(recipientLabel, constraints);

        recipientField = new JTextField();
        constraints.gridx = 1;
        constraints.gridy = 0;
        add(recipientField, constraints);

        JButton okButton = new JButton("Send");
        okButton.addActionListener(this);
        constraints.gridx = 0;
        constraints.gridy = 1;
        constraints.gridwidth = 2;
        constraints.weightx = 1.0;
        constraints.insets = new Insets(2, 5, 2, 5);
        add(okButton, constraints);

        pack();
        setLocationRelativeTo(null);
        setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        kindleDeviceEmail = recipientField.getText();
        if (isValidEmail(kindleDeviceEmail)) {
            emailDataEntered = true;
            performMailSending();
            dispose();
        } else {
            JOptionPane.showMessageDialog(this, "Please enter valid email data", "Invalid Data", JOptionPane.ERROR_MESSAGE);
        }
    }

    private boolean isValidEmail(String email) {
        String emailRegex = "^[\\w.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        return email.toUpperCase().matches(emailRegex);
    }

    private void performMailSending() {
        try {
            new EmailSenderImpl().sendEmail(new GmailOAuth2().authenticate());
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }
}