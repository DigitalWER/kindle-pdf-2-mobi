package com.marcin.kindle.converter.windows;

import com.marcin.kindle.converter.configuration.Converter;
import com.marcin.kindle.converter.configuration.impl.ConverterImpl;
import lombok.extern.slf4j.Slf4j;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.WindowConstants;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import static com.marcin.kindle.converter.context.FileContext.inputFileDirectory;
import static com.marcin.kindle.converter.context.FileContext.outputDirectory;

@Slf4j
public class MainWindow extends JFrame implements ActionListener {

    private final JTextField inputFileDirectoryField;
    private final JTextField outputDirectoryField;
    private final Converter converter = new ConverterImpl();

    public MainWindow() {
        setTitle("File ConverterImpl");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setLayout(new GridBagLayout());
        setWindowsLookToApp();
        setPreferredSize(new Dimension(600, 150));

        GridBagConstraints windowConstraints = new GridBagConstraints();
        windowConstraints.fill = GridBagConstraints.HORIZONTAL;
        windowConstraints.insets = new Insets(5, 5, 5, 5);

        JLabel inputLabel = new JLabel("Input File Path:");
        windowConstraints.gridx = 0;
        windowConstraints.gridy = 0;
        add(inputLabel, windowConstraints);

        inputFileDirectoryField = new JTextField();
        windowConstraints.gridx = 1;
        windowConstraints.gridy = 0;
        windowConstraints.weightx = 1.0;
        add(inputFileDirectoryField, windowConstraints);

        JButton inputFileButton = new JButton("File  ->");
        inputFileButton.addActionListener(this);
        windowConstraints.gridx = 2;
        windowConstraints.gridy = 0;
        windowConstraints.weightx = 0.0;
        add(inputFileButton, windowConstraints);

        JLabel outputLabel = new JLabel("Output Directory:");
        windowConstraints.gridx = 0;
        windowConstraints.gridy = 1;
        add(outputLabel, windowConstraints);

        outputDirectoryField = new JTextField();
        windowConstraints.gridx = 1;
        windowConstraints.gridy = 1;
        windowConstraints.weightx = 1.0;
        add(outputDirectoryField, windowConstraints);

        JButton outputDirectoryButton = new JButton("Output ->");
        outputDirectoryButton.addActionListener(this);
        windowConstraints.gridx = 2;
        windowConstraints.gridy = 1;
        windowConstraints.weightx = 0.0;
        add(outputDirectoryButton, windowConstraints);

        JButton actionButton = new JButton("Do Action");
        actionButton.addActionListener(this);
        windowConstraints.gridx = 0;
        windowConstraints.gridy = 2;
        windowConstraints.gridwidth = 3;
        windowConstraints.weightx = 1.0;
        windowConstraints.insets = new Insets(10, 5, 5, 5);
        add(actionButton, windowConstraints);

        pack();
        setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String actionCommand = e.getActionCommand();
        switch (actionCommand) {
            case "File  ->" -> selectInputFileMethod();
            case "Output ->" -> selectOutputDirectoryMethod();
            case "Do Action" -> performConversion();
        }
    }

    private void selectInputFileMethod() {
        log.info("Selecting input file.");
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setDialogTitle("Select Input File");
        fileChooser.setFileFilter(new FileNameExtensionFilter("PDF Files", "pdf"));
        openAndChooseDir(fileChooser, inputFileDirectoryField);
        inputFileDirectory = inputFileDirectoryField.getText().trim();
        log.info("Input file selected: {}", inputFileDirectory);
    }

    private void selectOutputDirectoryMethod() {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setDialogTitle("Select Output Directory");
        fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        openAndChooseDir(fileChooser, outputDirectoryField);
        outputDirectory = outputDirectoryField.getText().trim();
        log.info("Output file directory selected: {}", outputDirectory);
    }

    private void openAndChooseDir(JFileChooser fileChooser, JTextField field) {
        int result = fileChooser.showOpenDialog(this);
        if (result == JFileChooser.APPROVE_OPTION) {
            File selectedDirectory = fileChooser.getSelectedFile();
            field.setText(selectedDirectory.getAbsolutePath());
        }
    }

    private void performConversion() {
        if (isFieldEmpty(inputFileDirectoryField) || isFieldEmpty(outputDirectoryField)) {
            JOptionPane.showMessageDialog(this,
                    "Fields cannot be empty!",
                    "Blank fields",
                    JOptionPane.ERROR_MESSAGE);
            return;
        }

        if (!isValidFile(inputFileDirectory) || !isValidDirectory(outputDirectory)) {
            JOptionPane.showMessageDialog(this,
                    "Paths are not valid, files might not exist!",
                    "Invalid path",
                    JOptionPane.ERROR_MESSAGE);
            return;
        }

        converter.pdfToMobi();
        int option = JOptionPane.showConfirmDialog(this,
                "Do you want to send the converted file via email?",
                "Email Sender",
                JOptionPane.YES_NO_OPTION);
        if (option == JOptionPane.YES_OPTION) {
            log.info("Chosen option: Yes, Input your email data: ");
            new EmailWindow();
        } else {
            log.info("Chosen option: No, File won't be send by email.");
        }
    }

    private boolean isValidFile(String path) {
        File file = new File(path);
        return file.exists() && !file.isDirectory();
    }

    private boolean isValidDirectory(String path) {
        File directory = new File(path);
        return directory.exists() && directory.isDirectory();
    }

    private boolean isFieldEmpty(JTextField field) {
        return field.getText().equals("");
    }

    public void setWindowsLookToApp() {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
