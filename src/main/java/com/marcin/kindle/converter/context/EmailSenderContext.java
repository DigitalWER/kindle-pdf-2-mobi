package com.marcin.kindle.converter.context;

public class EmailSenderContext {
    public static String kindleDeviceEmail;
    public static final String CREDENTIALS_FILE_PATH = "creds.json";
    public static final String TOKENS_DIRECTORY_PATH = "classpath";
}
