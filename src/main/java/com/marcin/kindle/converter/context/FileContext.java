package com.marcin.kindle.converter.context;

import java.io.File;

public class FileContext {
    public static String inputFileDirectory;
    public static String outputDirectory;
    public static String outputFile;

    public static String getFullFile(File fileDir) {
        return fileDir.getName();
    }

    public static String getFileName(String file) {
        return file.substring(0, file.lastIndexOf("."));
    }
}
